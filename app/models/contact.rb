# frozen_string_literal: true

class Contact < ApplicationRecord
  enum origin: %i[website email phone_call]

  has_rich_text :message

  validates :email, :name, :message, presence: true

  def contact_origin
    @contact_origin ||=
      case origin
      when "website"
        "from the website"
      when "email"
        "by email"
      else
        "by phone"
      end
  end
end

# == Schema Information
#
# Table name: contacts
#
#  id           :bigint           not null, primary key
#  email        :string           not null
#  name         :string           not null
#  origin       :integer          default("website")
#  phone_number :string
#  created_at   :datetime         not null
#  updated_at   :datetime         not null
#
