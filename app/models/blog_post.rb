# frozen_string_literal: true

class BlogPost < ApplicationRecord
  extend FriendlyId
  friendly_id :title, use: :slugged

  enum status: %i[draft published archived]

  belongs_to :user

  has_and_belongs_to_many :tags

  has_rich_text :body

  validates :body, presence: true
  validates :title, presence: true, uniqueness: true

  accepts_nested_attributes_for :tags, reject_if: lambda { |attrs| attrs["name"].blank? }

  scope :for_user, ->(user_id) { where(user_id: user_id) }

  validate :set_published_at

  def should_generate_new_friendly_id?
    title_changed?
  end

  private

  def set_published_at
    update_column(:published_at, DateTime.now) if status_changed? && published?
  end
end

# == Schema Information
#
# Table name: blog_posts
#
#  id           :bigint           not null, primary key
#  published_at :datetime
#  slug         :string
#  status       :integer          default("draft")
#  title        :string
#  created_at   :datetime         not null
#  updated_at   :datetime         not null
#  user_id      :bigint           not null
#
# Indexes
#
#  index_blog_posts_on_slug     (slug) UNIQUE
#  index_blog_posts_on_user_id  (user_id)
#
# Foreign Keys
#
#  fk_rails_...  (user_id => users.id)
#
