# frozen_string_literal: true

class Technology < ApplicationRecord
  has_and_belongs_to_many :portfolios

  validates :name, presence: true, uniqueness: true
end

# == Schema Information
#
# Table name: technologies
#
#  id         :bigint           not null, primary key
#  name       :string
#  created_at :datetime         not null
#  updated_at :datetime         not null
#
