# frozen_string_literal: true

class Portfolio < ApplicationRecord
  include Placeholder

  extend FriendlyId
  friendly_id :title, use: :slugged

  validates_presence_of :title, :subtitle, :body, :position

  belongs_to :user

  has_and_belongs_to_many :technologies

  scope :for_user, ->(user_id) { where(user_id: user_id) }

  has_one_attached :image do |attachable|
    attachable.variant :thumb, resize_to_limit: [300, 200]
  end

  has_many_attached :photos

  accepts_nested_attributes_for :technologies, reject_if: lambda { |attrs| attrs["name"].blank? }

  scope :published, -> { where(published: true) }

  def should_generate_new_friendly_id?
    title_changed?
  end
end

# == Schema Information
#
# Table name: portfolios
#
#  id           :bigint           not null, primary key
#  body         :text
#  live_link    :string
#  position     :integer
#  published    :boolean          default(FALSE)
#  slug         :string
#  staging_link :string
#  subtitle     :string
#  title        :string
#  created_at   :datetime         not null
#  updated_at   :datetime         not null
#  user_id      :bigint           not null
#
# Indexes
#
#  index_portfolios_on_slug     (slug) UNIQUE
#  index_portfolios_on_user_id  (user_id)
#
# Foreign Keys
#
#  fk_rails_...  (user_id => users.id)
#
