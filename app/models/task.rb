# frozen_string_literal: true

class Task < ApplicationRecord
  belongs_to :created_by, class_name: "User"
  belongs_to :assigned_to, class_name: "User"

  scope :for_user, ->(user) { where(assigned_to: user) }
end

# == Schema Information
#
# Table name: tasks
#
#  id             :bigint           not null, primary key
#  completed      :boolean          default(FALSE)
#  description    :string
#  created_at     :datetime         not null
#  updated_at     :datetime         not null
#  assigned_to_id :bigint           not null
#  created_by_id  :bigint           not null
#
# Indexes
#
#  index_tasks_on_assigned_to_id  (assigned_to_id)
#  index_tasks_on_created_by_id   (created_by_id)
#
# Foreign Keys
#
#  fk_rails_...  (assigned_to_id => users.id)
#  fk_rails_...  (created_by_id => users.id)
#
