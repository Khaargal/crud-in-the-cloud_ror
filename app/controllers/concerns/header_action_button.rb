# frozen_string_literal: true

module HeaderActionButton
  extend ActiveSupport::Concern

  included do
    before_action :set_btn_link
  end

  def set_btn_link
    if %w[tasks technologies pages].include?(controller_name)
      @set_btn_link ||= ["", "", ""]
    else
      @set_btn_link ||=
        case params[:action]
        when "index"
          %W[New /#{params[:controller]}/new submit-btn]
        when "show"
          %W[Edit /#{params[:controller]}/#{params[:id]}/edit edit-btn]
        when "new"
          %W[Cancel /#{params[:controller]} cancel-btn]
        when "edit"
          %W[Cancel /#{params[:controller]}/#{params[:id]} cancel-btn]
        else
          ""
        end
    end
  end
end