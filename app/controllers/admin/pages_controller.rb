# frozen_string_literal: true

class Admin::PagesController < ApplicationController
  layout "admin"

  before_action :authenticate_admin

  def home
  end

  private

  def authenticate_admin
    if !current_user.admin?
      redirect_to dashboard_path, alert: "No can do buddy!"
    end
  end
end
