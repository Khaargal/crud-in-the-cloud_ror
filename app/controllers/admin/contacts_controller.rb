# frozen_string_literal: true

class Admin::ContactsController < ApplicationController
  layout "admin"

  before_action :authenticate_user!
  before_action :set_contact, only: %i[show edit update destroy]

  def index
    @contacts = Contact.all
  end

  def new
    @contact = Contact.new
  end

  def edit
  end

  def create
    @contact = Contact.new(contact_params)
    if @contact.save
      redirect_to admin_contact_path(@contact), notice: "contact created!"
    else
      render :new, status: :unprocessable_entity
    end
  end

  def update
    if @contact.update(contact_params)
      redirect_to admin_contact_path(@contact), notice: "The contact has been updated."
    else
      render :edit, status: :unprocessable_entity
    end
  end

  def destroy
    if @contact.destroy
      redirect_to admin_contacts_path, notice: "The contact has been deleted."
    else
      redirect_to admin_contacts_path, alert: "The contact has NOT been deleted."
    end
  end

  private

  def set_contact
    @contact = Contact.find(params[:id])
  end

  def contact_params
    params.require(:contact).permit(
      :name,
      :email,
      :phone_number,
      :message
    )
  end
end
