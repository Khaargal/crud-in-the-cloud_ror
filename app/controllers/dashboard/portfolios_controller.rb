# frozen_string_literal: true

module Dashboard
  class PortfoliosController < ApplicationController
    layout "dashboard"

    before_action :authenticate_user!
    before_action :set_portfolio, only: [:show, :edit, :update, :destroy]

    def index
      @portfolios = Portfolio.for_user(current_user.id).includes([:technologies]).order(:position)
    end

    def new
      @portfolio = Portfolio.new
    end

    def create
      @portfolio = Portfolio.new(portfolio_params)

      if @portfolio.save
        redirect_to dashboard_portfolios_path, notice: "Your Portfolio has been added."
      else
        render :new, status: :unprocessable_entity
      end
    end

    def edit
    end

    def show
    end

    def update
      respond_to do |format|
        if @portfolio.update(portfolio_params)
          format.html { redirect_to dashboard_portfolio_path(@portfolio), notice: "Your Portfolio has been updated." }
        else
          format.html { render :edit }
        end
      end
    end

    def destroy
      @portfolio.destroy
      respond_to do |format|
        format.html { redirect_to dashboard_portfolios_path, notice: "The Portfolio has been deleted!" }
      end
    end

    private

    def set_portfolio
      @portfolio = Portfolio.friendly.find(params[:id])
    end

    def portfolio_params
      params.require(:portfolio).permit(
        :title,
        :subtitle,
        :body,
        :image,
        :live_link,
        :staging_link,
        :published,
        :position,
        :user_id,
        technology_ids: [],
        technologies_attributes: [:name]
      )
    end
  end
end
