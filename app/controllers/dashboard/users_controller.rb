# frozen_string_literal: true

class Dashboard::UsersController < ApplicationController
  layout "dashboard"

  before_action :authenticate_user!
  before_action :set_user, only: %i[show edit update destroy]
  before_action :authorized_users, only: %i[edit update destroy]

  def show
  end

  def edit
  end
  
  def update
    if @user.update(user_params)
      redirect_to dashboard_user_path(@user), notice: "The user has been updated."
    else
      render :edit, status: :unprocessable_entity
    end
  end

  def destroy
    if @user.destroy
      redirect_to root_path, notice: "The user has been deleted."
    else
      redirect_to dashboard_users_path, alert: "The user has NOT been deleted."
    end
  end

  private

  def authorized_users
    unless current_user == @user
      redirect_to dashboard_path, alert: "No can do buddy."
    end
  end

  def set_user
    @user = User.find(params[:id])
  end

  def user_params
    params.require(:user).permit(
      :avatar,
      :first_name,
      :last_name,
      :email,
      :password,
      :password_confirmation,
    )
  end
end
