# frozen_string_literal: true

class Dashboard::BlogPostsController < ApplicationController
  layout "dashboard"

  before_action :authenticate_user!
  before_action :set_blog_post, only: %i[show edit update destroy]

  def index
    @blog_posts = BlogPost.for_user(current_user.id)
  end

  def show
  end

  def new
    @blog_post = BlogPost.new
  end

  def edit
  end

  def create
    @blog_post = BlogPost.new(blog_post_params)

    if @blog_post.save
      redirect_to dashboard_blog_posts_path, notice: "Blog post created!"
    else
      render :new, status: :unprocessable_entity
    end
  end

  def update
    if @blog_post.update(blog_post_params)
      redirect_to dashboard_blog_post_path(@blog_post), notice: "The blog post has been updated."
    else
      render :edit, status: :unprocessable_entity
    end
  end

  def destroy
    if @blog_post.destroy
      redirect_to dashboard_blog_posts_path, notice: "The blog_post has been deleted."
    else
      redirect_to dashboard_blog_posts_path, alert: "The blog_post has NOT been deleted."
    end
  end

  private

  def set_blog_post
    @blog_post = BlogPost.friendly.find(params[:id])
  end

  def blog_post_params
    params.require(:blog_post).permit(
      :title,
      :body,
      :status,
      :user_id,
      tag_ids: [],
      tags_attributes: [:name]
    )
  end
end
