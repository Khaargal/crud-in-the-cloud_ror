# frozen_string_literal: true

module Dashboard
  class TechnologiesController < ApplicationController
    layout "dashboard"

    before_action :authenticate_user!
    before_action :set_technology, only: [:show, :edit, :update, :destroy]

    def index
      @technologies = Technology.all
    end

    def show
    end

    def edit
    end

    def create
      @technology = Technology.new(technology_params)
      if @technology.save
        redirect_back fallback_location: dashboard_technologies_path, notice: "The Technology has been added."
      else
        redirect_back fallback_location: dashboard_technologies_path, alert: "The Technology has NOT been added."
      end

    end

    def update
      @technology.update!(technology_params)
    end

    def destroy
      @technology.destroy!
    end

    private

    def set_technology
      @technology = Technology.find(params[:id])
    end

    def technology_params
      params.require(:technology).permit(:name)
    end
  end
end
