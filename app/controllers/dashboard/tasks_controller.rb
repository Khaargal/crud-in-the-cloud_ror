# frozen_string_literal: true

class Dashboard::TasksController < ApplicationController
  layout "dashboard"

  before_action :authenticate_user!
  before_action :set_task, only: [:update, :destroy]

  def index
    @tasks = Task.for_user(current_user)
  end

  def create
    @task = Task.create(task_params)
    @task.created_by = current_user
    @task.assigned_to = current_user
    @task.save
  end

  def update
    @task.update!(task_params)
  end

  def destroy
    @task.destroy!
  end

  private

  def set_task
    @task = Task.find(params[:id])
  end

  def task_params
    params.require(:task).permit(:description, :completed, :created_by, :assigned_to)
  end
end