# frozen_string_literal: true

class Dashboard::TagsController < ApplicationController
  layout "dashboard"

  before_action :authenticate_user!
  before_action :set_tag, only: [:update, :destroy]

  def index
    @tags = Tag.all
  end

  def create
    @tag = Tag.new(tag_params)
    @tag.save
  end

  def update
    @tag.update!(tag_params)
  end

  def destroy
    @tag.destroy!
  end

  private

  def set_tag
    @tag = Tag.find(params[:id])
  end

  def tag_params
    params.require(:tag).permit(:name)
  end
end