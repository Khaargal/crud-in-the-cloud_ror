# frozen_string_literal: true

class ApplicationController < ActionController::Base
  include HeaderActionButton

  protected

  def after_sign_in_path_for(resource)
    # Customize the redirect path based on the resource type
    if resource.is_a?(User)
      # Redirect to a specific path for users
      # For example, root_path, dashboard_path, etc.
      dashboard_path
    else
      # Redirect to a different path for other resources (if needed)
      super
    end
  end
end
