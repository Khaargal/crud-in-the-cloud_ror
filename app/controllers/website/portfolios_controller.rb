# frozen_string_literal: true

module Website
  class PortfoliosController < ApplicationController
    before_action :set_portfolio, only: [:show]

    def index
      @portfolios = Portfolio.published.includes([:technologies]).order(:position)
    end

    def show
    end

    private

    def set_portfolio
      @portfolio = Portfolio.friendly.find(params[:id])
    end
  end
end
