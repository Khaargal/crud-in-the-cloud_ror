# frozen_string_literal: true

class Website::PagesController < ApplicationController
  def homepage
  end
end
