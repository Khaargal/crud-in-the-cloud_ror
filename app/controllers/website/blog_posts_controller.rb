# frozen_string_literal: true

class Website::BlogPostsController < ApplicationController

  def index
    @blog_posts = BlogPost.published
  end

  def show
    @blog_post = BlogPost.friendly.find(params[:id])
  end
end
