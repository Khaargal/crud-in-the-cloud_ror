# frozen_string_literal: true

class Website::ContactsController < ApplicationController
  def new
    @contact = Contact.new
  end

  def create
    @contact = Contact.new(contact_params)
    if @contact.save
      redirect_to root_path, notice: "Message sent. I will get back to you as soon as possible."
    else
      render :new, status: :unprocessable_entity
    end
  end

  private

  def contact_params
    params.require(:contact).permit(
      :name,
      :email,
      :phone_number,
      :origin,
      :message
    )
  end
end
