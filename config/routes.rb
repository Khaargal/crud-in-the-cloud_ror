Rails.application.routes.draw do
  resources :tasks
  # Reveal health status on /up that returns 200 if the app boots with no exceptions, otherwise 500.
  # Can be used by load balancers and uptime monitors to verify that the app is live.
  get "up" => "rails/health#show", as: :rails_health_check

  devise_for :users

  # Defines the root path route ("/")
  root "website/pages#homepage"

  namespace :admin, path: "/admin" do
    get "/", to: "pages#home"
    resources :contacts
    resources :users
  end

  namespace :dashboard, path: "/dashboard" do
    get "/", to: "pages#home"
    resources :blog_posts
    resources :portfolios
    resources :tags
    resources :tasks
    resources :technologies
    resources :users, only: %i[show edit]
  end

  namespace :website, path: "/" do
    get "/", to: "pages#homepage"
    get    "/blog"          => "blog_posts#index"
    get    "/blog/:id"      => "blog_posts#show", as: :show_blog
    resources :contacts, only: %i[new create]
    resources :portfolios, only: %i[index show]
  end

  # ATTACHMENTS
  delete "attachments/:id/purge", to: "attachments#purge", as: :purge_attachment
  get "attachments/:id", to: "attachments#show", as: :show_attachment
end
