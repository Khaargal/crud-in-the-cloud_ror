# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Contact, type: :model do
  let(:contact) { create(:contact) }

  it "is valid with valid attributes" do
    expect(contact).to be_valid
  end
end

# == Schema Information
#
# Table name: contacts
#
#  id           :bigint           not null, primary key
#  email        :string           not null
#  name         :string           not null
#  origin       :integer          default("website")
#  phone_number :string
#  created_at   :datetime         not null
#  updated_at   :datetime         not null
#
