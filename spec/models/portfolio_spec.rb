require "rails_helper"

RSpec.describe Portfolio, type: :model do
  let(:portfolio) { build(:portfolio) }

  it "has a valid factory" do
    expect(portfolio).to be_valid
  end

  describe "associations" do
    it { should belong_to(:user).class_name('User') }
  end
end

# == Schema Information
#
# Table name: portfolios
#
#  id           :bigint           not null, primary key
#  body         :text
#  live_link    :string
#  position     :integer
#  published    :boolean          default(FALSE)
#  slug         :string
#  staging_link :string
#  subtitle     :string
#  title        :string
#  created_at   :datetime         not null
#  updated_at   :datetime         not null
#  user_id      :bigint           not null
#
# Indexes
#
#  index_portfolios_on_slug     (slug) UNIQUE
#  index_portfolios_on_user_id  (user_id)
#
# Foreign Keys
#
#  fk_rails_...  (user_id => users.id)
#
