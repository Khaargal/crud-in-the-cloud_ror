require "rails_helper"

RSpec.describe Technology, type: :model do
  let(:technology) { build(:technology) }

  it "has a valid factory" do
    expect(technology).to be_valid
  end

  it "is does not accept duplicates" do
    create(:technology, name: "The second technology")
    new_technology = build(:technology, name: "The second technology")
    expect(new_technology).not_to be_valid
  end
end

# == Schema Information
#
# Table name: technologies
#
#  id         :bigint           not null, primary key
#  name       :string
#  created_at :datetime         not null
#  updated_at :datetime         not null
#
