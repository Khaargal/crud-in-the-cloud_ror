require "rails_helper"

RSpec.describe BlogPost, type: :model do
  subject { build(:blog_post) }
  let(:second_blog_post) { create(:blog_post) }


  it "has a valid factory" do
    expect(second_blog_post).to be_valid
  end

  describe "validation" do
    it { is_expected.to validate_presence_of(:title) }
    it { is_expected.to validate_uniqueness_of(:title) }
    it { is_expected.to validate_presence_of(:body) }
  end

  describe "associations" do
    it { should belong_to(:user).class_name('User') }
  end
end

# == Schema Information
#
# Table name: blog_posts
#
#  id           :bigint           not null, primary key
#  published_at :datetime
#  slug         :string
#  status       :integer          default("draft")
#  title        :string
#  created_at   :datetime         not null
#  updated_at   :datetime         not null
#  user_id      :bigint           not null
#
# Indexes
#
#  index_blog_posts_on_slug     (slug) UNIQUE
#  index_blog_posts_on_user_id  (user_id)
#
# Foreign Keys
#
#  fk_rails_...  (user_id => users.id)
#
