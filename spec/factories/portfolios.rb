# frozen_string_literal: true

FactoryBot.define do
  factory :portfolio do
    title { "Portfolio number #{1}" }
    subtitle { "Ruby On Rails" }
    body { Faker::Lorem.paragraph_by_chars(number: 110, supplemental: false)}
    position { 1 }
    published { true }
    live_link { "http://localhost:3000/" }
    staging_link { "http://localhost:3000/" }
    user { association(:user) }
  end
end

# == Schema Information
#
# Table name: portfolios
#
#  id           :bigint           not null, primary key
#  body         :text
#  live_link    :string
#  position     :integer
#  published    :boolean          default(FALSE)
#  slug         :string
#  staging_link :string
#  subtitle     :string
#  title        :string
#  created_at   :datetime         not null
#  updated_at   :datetime         not null
#  user_id      :bigint           not null
#
# Indexes
#
#  index_portfolios_on_slug     (slug) UNIQUE
#  index_portfolios_on_user_id  (user_id)
#
# Foreign Keys
#
#  fk_rails_...  (user_id => users.id)
#
