# frozen_string_literal: true

FactoryBot.define do
  factory :blog_post do
    title { Faker::Lorem.sentence(word_count: 3, supplemental: true, random_words_to_add: 4) }
    body {  Faker::Lorem.paragraph_by_chars(number: 756, supplemental: false) }
    user { association(:user) }
  end
end

# == Schema Information
#
# Table name: blog_posts
#
#  id           :bigint           not null, primary key
#  published_at :datetime
#  slug         :string
#  status       :integer          default("draft")
#  title        :string
#  created_at   :datetime         not null
#  updated_at   :datetime         not null
#  user_id      :bigint           not null
#
# Indexes
#
#  index_blog_posts_on_slug     (slug) UNIQUE
#  index_blog_posts_on_user_id  (user_id)
#
# Foreign Keys
#
#  fk_rails_...  (user_id => users.id)
#
