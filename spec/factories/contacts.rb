# frozen_string_literal: true

FactoryBot.define do
  factory :contact do
    name { Faker::Name.last_name }
    email { Faker::Internet.email }
    message { "Hello, whould like to suggest a collaboration on..." }
  end
end

# == Schema Information
#
# Table name: contacts
#
#  id           :bigint           not null, primary key
#  email        :string           not null
#  name         :string           not null
#  origin       :integer          default("website")
#  phone_number :string
#  created_at   :datetime         not null
#  updated_at   :datetime         not null
#
