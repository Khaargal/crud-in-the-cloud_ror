FactoryBot.define do
  factory :tag do
    sequence(:name) { |n| "tag name#{n}" }
  end
end

# == Schema Information
#
# Table name: tags
#
#  id         :bigint           not null, primary key
#  name       :string
#  created_at :datetime         not null
#  updated_at :datetime         not null
#
