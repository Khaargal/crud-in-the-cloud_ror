require "rails_helper"

RSpec.describe "Homepage" do
  it "renders homepage" do
    visit root_path

    expect(page).to have_content "Hello 👋, I am"
    expect(page).to have_content "Projects"
  end
end
