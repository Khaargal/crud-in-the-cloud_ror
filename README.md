# Crud-in-the-Cloud

- ruby v3.3.0
- node v20.11.0
- bundler v2.5.6
- postgresql
- redis
- vips

## Setting up your work environment

### To install the application dependencies

- Install bundler

```sh
❯ gem install bundler -v 2.5.3
```

- Ruby:

```sh
❯ bundle install
```

- Javascript:

```sh
❯ yarn install
```

- vips (for image processing)

```sh
❯ brew install vips
```

### Prepare the database

- Create the database, run:

```sh
❯ bundle exec rails db:create
```

- Run migration:

```sh
❯ bundle exec rails db:schema:load
```

- Generate data:

```sh
❯ bundle exec rails db:seed
```

### Running the app

Start your server:
`bin/dev` this will launch all the processes listed in `Procfile.dev`

Enjoy the headaches 😁

### Links

- All routes

[http://localhost:3000/rails/info/routes](http://localhost:3000/rails/info/routes)

## Specs

* Run all specs

```sh
❯ bundle exec rspec
```

* Run all specs and show browser for System specs

```sh
❯ SHOW_CHROME=1 bundle exec rspec
```

## Database

* Connect to the development database
  * ```sh
    ❯ bundle exec rails db
    ```
    Might take some time to load depending on your machine's power.
  * ```sh
    ❯ pqsl crud_in_the_cloud_ror_development 
    ``` 

## Gotchas & Known Issues

### binding.irb, binding.pry

When running the app with `$ bin/dev` and then try to debug in the CLI after setting a breakpoint, `binding.pry`, in the
code it will not work properly, better to run the app using `$ bin/rails server -p 7000` in that case.
The reason is that `$ bin/dev` uses the _Procfile.dev_ which not only runs the rails server but also JS & CSS watcher.

### Restart services

❗ Might need to launch additional commands for Redis to work:

```sh
❯ brew services restart postgresql
```

```sh
❯ brew services restart redis
```
