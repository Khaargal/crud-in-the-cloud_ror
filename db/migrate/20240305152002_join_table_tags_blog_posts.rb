class JoinTableTagsBlogPosts < ActiveRecord::Migration[7.1]
  def change
    create_join_table :tags, :blog_posts
  end
end
