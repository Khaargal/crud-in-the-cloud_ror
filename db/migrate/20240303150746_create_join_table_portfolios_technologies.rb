class CreateJoinTablePortfoliosTechnologies < ActiveRecord::Migration[7.1]
  def change
    create_join_table :portfolios, :technologies
  end
end
