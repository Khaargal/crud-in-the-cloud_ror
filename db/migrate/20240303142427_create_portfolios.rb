class CreatePortfolios < ActiveRecord::Migration[7.1]
  def change
    create_table :portfolios do |t|
      t.belongs_to :user, index: true, foreign_key: true, null: false
      t.string :title
      t.string :subtitle
      t.text :body
      t.boolean :published, default: false
      t.string :live_link
      t.string :staging_link
      t.integer :position

      t.timestamps
    end
  end
end
