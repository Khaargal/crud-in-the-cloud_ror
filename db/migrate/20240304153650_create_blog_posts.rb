class CreateBlogPosts < ActiveRecord::Migration[7.1]
  def change
    create_table :blog_posts do |t|
      t.belongs_to :user, index: true, foreign_key: true, null: false
      t.string :title
      t.datetime :published_at
      t.integer :status, default: 0

      t.timestamps
    end
  end
end
