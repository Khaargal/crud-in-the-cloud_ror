# This file should ensure the existence of records required to run the application in every environment (production,
# development, test). The code here should be idempotent so that it can be executed at any point in every environment.
# The data can then be loaded with the bin/rails db:seed command (or created alongside the database with db:setup).
#
# Example:
#
#   ["Action", "Comedy", "Drama", "Horror"].each do |genre_name|
#     MovieGenre.find_or_create_by!(name: genre_name)
#   end

Tag.destroy_all
BlogPost.destroy_all
Portfolio.destroy_all
Technology.destroy_all
Task.destroy_all
User.destroy_all

# Admins
first_user = User.create({ first_name: "John", last_name: "Smith", email: "jsmith@users.com", password: "password", admin: true })
first_user_id = first_user.id
User.create({ first_name: "Jake", last_name: "Davis", email: "jdavis@users.com", password: "password", admin: true })
# Users
User.create({ first_name: "Jane", last_name: "Smith", email: "janesmith@users.com", password: "password", admin: false })

# Tags
if Tag.count.zero?
  tag_names = ["Backend", "Frontend", "DevOps", "DevOpsSec", "Product Management", "Scrum", "Best Practices", "Carrier",
    "Agile", "Project Management", "Business", "Tools", "System", "Linux", "SecOps", "Support Engineering", "ITIL"
  ]
  tag_names.each { |tag_name| Tag.create(name: tag_name) }
end
first_tag_id = Tag.first.id

# Blog Posts
tag_count = 0

10.times do
  tag_count += 1
  BlogPost.create!(
    title: Faker::Lorem.sentence(word_count: 3, supplemental: true, random_words_to_add: 4),
    body: Faker::Lorem.paragraph_by_chars(number: 756, supplemental: false),
    user_id: first_user_id,
    tag_ids: tag_count.even? ? [first_tag_id, first_tag_id + 1, first_tag_id + 2, first_tag_id + 3] : [first_tag_id + 3, first_tag_id + 4, first_tag_id + 5, first_tag_id + 6]
  )
end

# Tasks
tasks_list = ["Improve Ruby vanilla skills", "Learn how engines work in Ruby on Rails", "Learn intermediary Javascript",
              "Learn NodeJS", "Learn Python", "Learn Django 101", "Learn the fundamentals of Docker", "Start learning Bash"]

tasks_list.each do |task|
  Task.create(description: task, assigned_to_id: first_user.id, created_by_id: first_user.id)
end

# Technologies
technologies_list = ["Ruby", "Ruby on Rails", "Javascript", "NodeJS", "Python", "Django", "Docker", "Bash"]

technologies_list.each do |tech|
  Technology.create(name: tech)
end

# Portfolio
if Portfolio.count.zero?
  count = 0
  first_technology_id = Technology.first.id

  10.times do |portfolio|
    Portfolio.create!(
      title: "Portfolio number #{portfolio + 1}",
      subtitle: Faker::Lorem.sentence(word_count: 3, supplemental: true, random_words_to_add: 4),
      body: Faker::Lorem.paragraph_by_chars(number: 110, supplemental: false),
      position: count += 1,
      published: count < 5 ? true : false,
      live_link: "http://localhost:3000/",
      staging_link: "http://localhost:3000/",
      user_id: count < 8 ? first_user_id : first_user_id + 1,
      technology_ids: count.even? ? [first_technology_id, first_technology_id + 1, first_technology_id + 2, first_technology_id + 3] : [first_technology_id + 3, first_technology_id + 4, first_technology_id + 5, first_technology_id + 6]
    )
  end
end